/*
***************************************************************************
*
* Author: Teunis van Beelen
*
* Copyright (C) 2024 Teunis van Beelen
*
* Email: teuniz@protonmail.com
*
***************************************************************************
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
***************************************************************************
*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <locale.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

#include "edflib.h"


#define  JUMP_TO_EXIT_ERROR_PROC   {line = __LINE__; goto OUT_ERROR;}

#if !defined(__GNUC__)
#error "you need the GNU C compiler!"
#endif

#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

typedef struct raw_header_struct
{
  int total_chans;
  int regular_chans;
  int annot_chans;
  int regular_chans_idx_list[EDFLIB_MAXSIGNALS];
  int annot_chans_idx_list[EDFLIB_MAXSIGNALS];
  int bytes_in_datrec[EDFLIB_MAXSIGNALS];
  int datrec_offset[EDFLIB_MAXSIGNALS];
  int hdr_sz;
  int datrecs;
  int datrec_sz;
  int sample_width;
  int is_edfplus;
  int is_bdfplus;
} raw_hdr_t;

int dblcmp(double, double);
int dblcmp_lim(double, double, double);
int get_raw_header(const char *, raw_hdr_t *);
int get_file_offset(int, int, int, raw_hdr_t *);


int main(void)
{
  int i, j,
      hdl=-1,
      *ibuf=NULL,
      line;

  char *str=NULL,
       *pbuf=NULL,
       *i3buf=NULL;

  short *sbuf=NULL;

  double *dbuf=NULL;

  raw_hdr_t rawhdr;

  edflib_hdr_t hdr;

  FILE *fp=NULL;

  setlocale(LC_ALL, "C");

  if(edflib_version() != 125)  JUMP_TO_EXIT_ERROR_PROC

  ibuf = (int *)malloc(50000 * sizeof(int));
  if(ibuf == NULL)
  {
    JUMP_TO_EXIT_ERROR_PROC;
  }

  sbuf = (short *)malloc(50000 * sizeof(short));
  if(sbuf == NULL)
  {
    JUMP_TO_EXIT_ERROR_PROC;
  }

  dbuf = (double *)malloc(50000 * sizeof(double));
  if(dbuf == NULL)
  {
    JUMP_TO_EXIT_ERROR_PROC;
  }

  str = (char *)malloc(4096 * sizeof(char));
  if(str == NULL)
  {
    JUMP_TO_EXIT_ERROR_PROC;
  }
  str[0] = 0;

  pbuf = (char *)malloc(300 * sizeof(char));
  if(pbuf == NULL)
  {
    JUMP_TO_EXIT_ERROR_PROC;
  }

  i3buf = (char *)malloc(50000 * 3);
  if(i3buf == NULL)
  {
    JUMP_TO_EXIT_ERROR_PROC;
  }

/********************************** BDF writing ******************************/

  hdl = edfopen_file_writeonly_with_params("test.bdf", EDFLIB_FILETYPE_BDFPLUS, 17, 2799, 300000, "uV");

  if(hdl < 0)  JUMP_TO_EXIT_ERROR_PROC

  if(edf_set_annot_chan_idx_pos(hdl, EDF_ANNOT_IDX_POS_START))  JUMP_TO_EXIT_ERROR_PROC

  if(edf_set_number_of_annotation_signals(hdl, 3))  JUMP_TO_EXIT_ERROR_PROC

  for(i=0; i<2799; i++)
  {
    dbuf[i] = i;
  }

  for(i=0; i<25; i++)
  {
    for(j=0; j<17; j++)
    {
      if(edfwrite_physical_samples(hdl, dbuf))  JUMP_TO_EXIT_ERROR_PROC
    }
  }

  for(i=0; i<75; i++)
  {
    snprintf(str, 4096, "test %i", i + 1);

    if(edfwrite_annotation_latin1_hr(hdl, i * 1000000, -1, str))  JUMP_TO_EXIT_ERROR_PROC
  }

  if(edfclose_file(hdl))
  {
    hdl = -1;

    JUMP_TO_EXIT_ERROR_PROC
  }

/********************************** BDF reading ******************************/

  if(get_raw_header("test.bdf", &rawhdr))  JUMP_TO_EXIT_ERROR_PROC

  if(rawhdr.annot_chans != 3)  JUMP_TO_EXIT_ERROR_PROC

  for(i=0; i<rawhdr.annot_chans; i++)
  {
    if(rawhdr.annot_chans_idx_list[i] != i)  JUMP_TO_EXIT_ERROR_PROC
  }

  if(edfopen_file_readonly("test.bdf", &hdr, EDFLIB_READ_ALL_ANNOTATIONS))  JUMP_TO_EXIT_ERROR_PROC

  hdl = hdr.handle;

  if(hdr.filetype != 3)  JUMP_TO_EXIT_ERROR_PROC

  if(hdr.edfsignals != 17)  JUMP_TO_EXIT_ERROR_PROC

  if(hdr.file_duration != 250000000)  JUMP_TO_EXIT_ERROR_PROC

  if(hdr.datarecord_duration != 10000000)  JUMP_TO_EXIT_ERROR_PROC

  if(hdr.datarecords_in_file != 25)  JUMP_TO_EXIT_ERROR_PROC

  if(hdr.signalparam[0].smp_in_file != 69975)  JUMP_TO_EXIT_ERROR_PROC

  if(hdr.signalparam[0].phys_max != 300000)  JUMP_TO_EXIT_ERROR_PROC

  if(hdr.signalparam[0].phys_min != -300000)  JUMP_TO_EXIT_ERROR_PROC

  if(hdr.signalparam[0].dig_max != 8388607)  JUMP_TO_EXIT_ERROR_PROC

  if(hdr.signalparam[0].dig_min != -8388608)  JUMP_TO_EXIT_ERROR_PROC

  if(hdr.signalparam[0].smp_in_datarecord != 2799)  JUMP_TO_EXIT_ERROR_PROC

  if(strcmp(hdr.signalparam[0].physdimension, "uV      "))  JUMP_TO_EXIT_ERROR_PROC

  if(strcmp(hdr.patientcode, ""))  JUMP_TO_EXIT_ERROR_PROC

  if(strcmp(hdr.patient_name, "X"))  JUMP_TO_EXIT_ERROR_PROC

  if(strcmp(hdr.admincode, ""))  JUMP_TO_EXIT_ERROR_PROC

  if(strcmp(hdr.technician, ""))  JUMP_TO_EXIT_ERROR_PROC

  if(strcmp(hdr.equipment, ""))  JUMP_TO_EXIT_ERROR_PROC

  if(hdr.annotations_in_file != 75)  JUMP_TO_EXIT_ERROR_PROC

  if(edfclose_file(hdl))
  {
    hdl = -1;

    JUMP_TO_EXIT_ERROR_PROC
  }

  hdl = -1;

/********************************** BDF invalidate, repair and read ******************************/

  if(get_raw_header("test.bdf", &rawhdr))  JUMP_TO_EXIT_ERROR_PROC

  fp = fopen("test.bdf", "r+b");
  if(fp == NULL)  JUMP_TO_EXIT_ERROR_PROC

  fseek(fp, 8, SEEK_SET);  /* reserved field */
  fwrite("hospitalc\0de X X abc\n" "\x7f" "fghij" "\xc0", 28, 1, fp);

  // fseek(fp, 168, SEEK_SET);  /* starttime field */
  // fputc('\0', fp);
  fseek(fp, 170, SEEK_SET);  /* starttime field */
  fputc('\0', fp);
  fseek(fp, 173, SEEK_SET);  /* starttime field */
  fputc(':', fp);

  fseek(fp, 178, SEEK_SET);  /* startdate field */
  fputc('/', fp);
  fseek(fp, 181, SEEK_SET);  /* startdate field */
  fputc('-', fp);

  fseek(fp, 200, SEEK_SET);  /* reserved field */
  fputc('1', fp);
  fputc('t', fp);
  fputc('\0', fp);
  fputc('\t', fp);
  fputc('\xd5', fp);

  fseek(fp, 254, SEEK_SET);  /* number of signals field */
  fputc('\0', fp);


  fseek(fp, 256 + (120 * rawhdr.total_chans) + (8 * rawhdr.regular_chans_idx_list[2]), SEEK_SET);  /* digmin field of regular signal 3 */
  fwrite("-8388609", 8, 1, fp);

  fseek(fp, 256 + (128 * rawhdr.total_chans) + (8 * rawhdr.regular_chans_idx_list[2]), SEEK_SET);  /* digmax field of regular signal 3 */
  fwrite("8388608 ", 8, 1, fp);

  fseek(fp, 256 + (120 * rawhdr.total_chans) + (8 * rawhdr.regular_chans_idx_list[3]), SEEK_SET);  /* digmin field of regular signal 4 */
  fwrite("0       ", 8, 1, fp);

  fseek(fp, 256 + (128 * rawhdr.total_chans) + (8 * rawhdr.regular_chans_idx_list[3]), SEEK_SET);  /* digmax field of regular signal 4 */
  fwrite("0       ", 8, 1, fp);

  fseek(fp, 256 + (120 * rawhdr.total_chans) + (8 * rawhdr.regular_chans_idx_list[4]), SEEK_SET);  /* digmin field of regular signal 5 */
  fwrite("8388607 ", 8, 1, fp);

  fseek(fp, 256 + (128 * rawhdr.total_chans) + (8 * rawhdr.regular_chans_idx_list[4]), SEEK_SET);  /* digmax field of regular signal 5 */
  fwrite("-8388608", 8, 1, fp);

  fclose(fp);

  if(system("../edf-hdr-repair.sh test.bdf"))  JUMP_TO_EXIT_ERROR_PROC

  if(edfopen_file_readonly("test.bdf", &hdr, EDFLIB_READ_ALL_ANNOTATIONS))  JUMP_TO_EXIT_ERROR_PROC

  hdl = hdr.handle;

  if(edfclose_file(hdl))
  {
    hdl = -1;

    JUMP_TO_EXIT_ERROR_PROC
  }

  hdl = -1;

/********************************** EDF writing ******************************/

  hdl = edfopen_file_writeonly_with_params("test.edf", EDFLIB_FILETYPE_EDFPLUS, 65, 633, 3000, "uV");

  if(hdl < 0)  JUMP_TO_EXIT_ERROR_PROC

  if(edf_set_annot_chan_idx_pos(hdl, EDF_ANNOT_IDX_POS_MIDDLE))  JUMP_TO_EXIT_ERROR_PROC

  for(j=0; j<65; j++)
  {
    for(i=0; i<633; i++)
    {
      dbuf[(j * 633) + i] = i;
    }
  }

  for(i=0; i<5; i++)
  {
    for(j=0; j<65; j++)
    {
      if(edfwrite_physical_samples(hdl, dbuf))  JUMP_TO_EXIT_ERROR_PROC
    }
  }

  for(i=0; i<5; i++)
  {
    if(edf_blockwrite_physical_samples(hdl, dbuf))  JUMP_TO_EXIT_ERROR_PROC
  }

  for(j=0; j<65; j++)
  {
    for(i=0; i<633; i++)
    {
      ibuf[i + (j * 633)] = (i * -10.92266667) + 0.5;
    }
  }

  for(i=0; i<5; i++)
  {
    for(j=0; j<65; j++)
    {
      if(edfwrite_digital_samples(hdl, ibuf))  JUMP_TO_EXIT_ERROR_PROC
    }
  }

  for(i=0; i<5; i++)
  {
    if(edf_blockwrite_digital_samples(hdl, ibuf))  JUMP_TO_EXIT_ERROR_PROC
  }

  for(j=0; j<65; j++)
  {
    for(i=0; i<633; i++)
    {
      sbuf[i + (j * 633)] = (i * -10.92266667) + 0.5;
    }
  }

  for(i=0; i<5; i++)
  {
    for(j=0; j<65; j++)
    {
      if(edfwrite_digital_short_samples(hdl, sbuf))  JUMP_TO_EXIT_ERROR_PROC
    }
  }

  for(i=0; i<5; i++)
  {
    if(edf_blockwrite_digital_short_samples(hdl, sbuf))  JUMP_TO_EXIT_ERROR_PROC
  }

  if(edfclose_file(hdl))
  {
    hdl = -1;

    JUMP_TO_EXIT_ERROR_PROC
  }

/********************************** EDF reading ******************************/

  if(edfopen_file_readonly("test.edf", &hdr, EDFLIB_READ_ALL_ANNOTATIONS))  JUMP_TO_EXIT_ERROR_PROC

  hdl = hdr.handle;

  if(hdr.filetype != 1)  JUMP_TO_EXIT_ERROR_PROC

  if(hdr.edfsignals != 65)  JUMP_TO_EXIT_ERROR_PROC

  if(hdr.file_duration != 300000000)  JUMP_TO_EXIT_ERROR_PROC

  if(hdr.datarecord_duration != 10000000)  JUMP_TO_EXIT_ERROR_PROC

  if(hdr.datarecords_in_file != 30)  JUMP_TO_EXIT_ERROR_PROC

  if(hdr.signalparam[0].smp_in_file != 18990)  JUMP_TO_EXIT_ERROR_PROC

  if(hdr.signalparam[0].phys_max != 3000)  JUMP_TO_EXIT_ERROR_PROC

  if(hdr.signalparam[0].phys_min != -3000)  JUMP_TO_EXIT_ERROR_PROC

  if(hdr.signalparam[0].dig_max != 32767)  JUMP_TO_EXIT_ERROR_PROC

  if(hdr.signalparam[0].dig_min != -32768)  JUMP_TO_EXIT_ERROR_PROC

  if(hdr.signalparam[0].smp_in_datarecord != 633)  JUMP_TO_EXIT_ERROR_PROC

  if(strcmp(hdr.signalparam[0].physdimension, "uV      "))  JUMP_TO_EXIT_ERROR_PROC

  if(strcmp(hdr.patientcode, ""))  JUMP_TO_EXIT_ERROR_PROC

  if(strcmp(hdr.patient_name, "X"))  JUMP_TO_EXIT_ERROR_PROC

  if(strcmp(hdr.admincode, ""))  JUMP_TO_EXIT_ERROR_PROC

  if(strcmp(hdr.technician, ""))  JUMP_TO_EXIT_ERROR_PROC

  if(strcmp(hdr.equipment, ""))  JUMP_TO_EXIT_ERROR_PROC

  if(edfclose_file(hdl))
  {
    hdl = -1;

    JUMP_TO_EXIT_ERROR_PROC
  }

  hdl = -1;

/********************************** EDF invalidate, repair and read ******************************/

  if(system("../edf-hdr-repair.sh test.edf"))  JUMP_TO_EXIT_ERROR_PROC

  if(edfopen_file_readonly("test.edf", &hdr, EDFLIB_READ_ALL_ANNOTATIONS))  JUMP_TO_EXIT_ERROR_PROC

  hdl = hdr.handle;

  if(edfclose_file(hdl))
  {
    hdl = -1;

    JUMP_TO_EXIT_ERROR_PROC
  }

  hdl = -1;

  /****************************************/

  free(ibuf);
  free(sbuf);
  free(dbuf);
  free(str);
  free(pbuf);
  free(i3buf);

  return EXIT_SUCCESS;

OUT_ERROR:

  if(hdl >= 0)
  {
    edfclose_file(hdl);
  }

  free(ibuf);
  free(sbuf);
  free(dbuf);
  free(str);
  free(pbuf);
  free(i3buf);

  fprintf(stderr, "Error, line %i file %s\n", line, __FILE__);

  return EXIT_FAILURE;
}


int dblcmp(double val1, double val2)
{
  long double diff = (long double)val1 - (long double)val2;

  if(diff > 1e-13)
  {
    return 1;
  }
  else if(-diff > 1e-13)
    {
      return -1;
    }
    else
    {
      return 0;
    }
}


int dblcmp_lim(double val1, double val2, double lim)
{
  long double diff = (long double)val1 - (long double)val2;

  if(diff > lim)
  {
    return 1;
  }
  else if(-diff > lim)
    {
      return -1;
    }
    else
    {
      return 0;
    }
}


int get_raw_header(const char *f_path, raw_hdr_t *hdr)
{
  int i, err=-999;

  char *buf=NULL,
       str[128]={""};

  FILE *f=NULL;

  if((f_path == NULL) || (hdr == NULL))  return -1;

  if(strlen(f_path) < 5)  return -2;

  memset(hdr, 0, sizeof(raw_hdr_t));

  buf = malloc(300);
  if(buf==NULL)
  {
    err = -3;
    goto EXIT_ERROR;
  }

  f = fopen(f_path, "rb");
  if(f==NULL)
  {
    err = -4;
    goto EXIT_ERROR;
  }

  if(fread(buf, 256, 1, f) != 1)
  {
    err = -11;
    goto EXIT_ERROR;
  }

  if(!memcmp(buf, "0       ", 8))
  {
    hdr->sample_width = 2;
  }
  else if(!memcmp(buf, "\xff" "BIOSEMI", 8))
    {
      hdr->sample_width = 3;
    }
    else
    {
      err = -11;
      goto EXIT_ERROR;
    }

  if((!memcmp(buf+192, "EDF+C", 5) || !memcmp(buf+192, "EDF+D", 5)) && (hdr->sample_width == 2))
  {
    hdr->is_edfplus = 1;
  }
  else if((!memcmp(buf+192, "BDF+C", 5) || !memcmp(buf+192, "BDF+D", 5)) && (hdr->sample_width == 3))
    {
      hdr->is_bdfplus = 1;
    }

  memcpy(str, buf+236, 8);
  str[8] = 0;
  hdr->datrecs = atoi(str);
  if(hdr->datrecs < 1)
  {
    err = -12;
    goto EXIT_ERROR;
  }

  memcpy(str, buf+252, 4);
  str[4] = 0;
  hdr->total_chans = atoi(str);
  if((hdr->total_chans < 1) || (hdr->total_chans > EDFLIB_MAXSIGNALS))
  {
    err = -13;
    goto EXIT_ERROR;
  }

  memcpy(str, buf+184, 8);
  str[8] = 0;
  hdr->hdr_sz = atoi(str);
  if((hdr->hdr_sz < 512) || (hdr->hdr_sz != ((hdr->total_chans + 1) * 256)))
  {
    err = -14;
    goto EXIT_ERROR;
  }

  free(buf);

  buf = malloc(hdr->hdr_sz);
  if(buf==NULL)
  {
    err = -15;
    goto EXIT_ERROR;
  }

  rewind(f);

  if(fread(buf, hdr->hdr_sz, 1, f) != 1)
  {
    err = -16;
    goto EXIT_ERROR;
  }

  for(i=0; i<hdr->total_chans; i++)
  {
    hdr->bytes_in_datrec[i] = atoi(buf+256+(hdr->total_chans*216)+(i*8)) * hdr->sample_width;

    hdr->datrec_offset[i] = hdr->datrec_sz;

    hdr->datrec_sz += hdr->bytes_in_datrec[i];

    if(hdr->sample_width == 2)
    {
      if(memcmp(buf+256+(i*16), "EDF Annotations ", 16))
      {
        hdr->regular_chans_idx_list[hdr->regular_chans] = i;

        hdr->regular_chans++;
      }
      else
      {
        hdr->annot_chans_idx_list[hdr->annot_chans] = i;

        hdr->annot_chans++;
      }
    }
    else
    {
      if(memcmp(buf+256+(i*16), "BDF Annotations ", 16))
      {
        hdr->regular_chans_idx_list[hdr->regular_chans] = i;

        hdr->regular_chans++;
      }
      else
      {
        hdr->annot_chans_idx_list[hdr->annot_chans] = i;

        hdr->annot_chans++;
      }
    }
  }

  if(!hdr->annot_chans)
  {
    err = -17;
    goto EXIT_ERROR;
  }

  if(f)  fclose(f);

  free(buf);

  return 0;

EXIT_ERROR:

  if(f)  fclose(f);

  free(buf);

  memset(hdr, 0, sizeof(raw_hdr_t));

//  printf("get_raw_header(): error: %i\n", err);

  return err;
}

/* datrec and chan are zero based
 * is_annot: 0: regular channel 1: annotation channel
 */
int get_file_offset(int datrec, int idx, int is_annot, raw_hdr_t *hdr)
{
  if((datrec < 0) || (datrec >= hdr->datrecs))  return -1;

  if(!hdr)  return -2;

  if((is_annot < 0) || (is_annot > 1))  return -3;

  if(is_annot)
  {
    if((idx < 0) || (idx >= hdr->annot_chans))  return -4;

    return (hdr->hdr_sz + (datrec * hdr->datrec_sz) + (hdr->datrec_offset[hdr->annot_chans_idx_list[idx]]));
  }
  else
  {
    if((idx < 0) || (idx >= hdr->regular_chans))  return -5;

    return (hdr->hdr_sz + (datrec * hdr->datrec_sz) + (hdr->datrec_offset[hdr->regular_chans_idx_list[idx]]));
  }
}













